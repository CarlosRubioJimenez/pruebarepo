package main;

public class Main {

	public static void main(String[] args) {
		int num1 = 5;
		int num2 = 10;
		int suma = num1 + num2;
		int resta = num1 - num2;
		int division= num1 / num2;
		int producto = num1 * num2;
		int resto = num1 % num2;
		
		System.out.println("La suma es: " + suma);
		System.out.println("La resta es: " + resta);
		System.out.println("La division es: " + division);
		System.out.println("El producto es: " + producto);
		System.out.println("El resto de la division es: " + resto);
		
	}

}
