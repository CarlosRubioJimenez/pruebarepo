package main;

public class Main {

	public static void main(String[] args) {
		//Convertir un n�mero a letra.
		
		byte numero = 68;
		char letra;
		
		letra = (char) numero;
		System.out.println("El s�mbolo ASCII del n�mero " + numero + " es " + letra);
	
		//Convertir de letra a n�mero.
		
		letra = 'z';
		numero = (byte) letra;
		System.out.println("El c�digo ASCII de la letra " + letra + " es " + numero);
	
	}

}
