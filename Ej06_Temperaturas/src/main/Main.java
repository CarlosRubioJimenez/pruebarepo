package main;

public class Main {

	public static void main(String[] args) {
		
		float Cen = 28.3f;
		float Rea = Cen * 0.8f;
		float Far = Cen * 1.8f + 32;
		float Kel = Cen + 273;
		
		System.out.println("Los grados Centígrados son: " + Cen);
		System.out.println("Los grados Reamhur son: " + Rea);
		System.out.println("Los grados Farenheit son: " + Far);
		System.out.println("Los grados Kelvin son: " + Kel);
		
	}

}
