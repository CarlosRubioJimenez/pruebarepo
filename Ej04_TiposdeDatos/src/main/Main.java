package main;

public class Main {
	
	//Secuencias de escape: nos permite pintar carcteres especiales.
	//	\n: enter.
	//	\t: tabulacion.
	//	\": pinta las comillas dobles.

	public static void main(String[] args) {
		//Enteros: byte, shotr, int, long.
		String nombre = "Carlos Rubio";
		byte edad=99;
		short sueldo = 30000;
		int sueldoanual = sueldo*12;
		
		//Decimales: float, double.
		float nota=5.6f;
		double media = 5.6789;
		
		System.out.println("Hola, " + nombre + ".");
		System.out.println("Tu edad es: " + edad + ".");
		System.out.println("Tu sueldo es: " + sueldo + ".");
		System.out.println("Tu sueldo anual es: " + sueldoanual + ".");
		System.out.println(nota + media);
	}

}
