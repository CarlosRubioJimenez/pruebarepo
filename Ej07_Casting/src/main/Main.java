package main;

public class Main {

	public static void main(String[] args) {
		int numeroInt = 1234567;
		byte numeroByte;
		
		numeroByte = (byte) numeroInt;
		System.out.println("Numero Byte = " + numeroByte);
		
		System.out.println("**conversion de decimales a enteros**");
		int numeroShort;
		float numeroFloat = 5.6f;
		System.out.println("Variable float = " + numeroFloat);
		numeroShort = (int) numeroFloat;
		System.out.println("Variable short = " + numeroShort);
		
		
	}

}
