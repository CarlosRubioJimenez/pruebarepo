package main;

public class Main {

	public static void main(String[] args) {
		
		float notaAlumno = 9.8f;
		byte notaProfesor;
		
		notaProfesor = (byte) Math.round(notaAlumno);
		System.out.println("La nota del alumno es " + notaAlumno + " y la nota del profesor es " + notaProfesor + ".");
		
	}

}
